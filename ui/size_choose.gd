extends VBoxContainer

var x : int = Global.pieces_x
var y : int = Global.pieces_y

func update_piece_counter():
	%PieceCounter.text = str(x * y)

func _ready():
	update_piece_counter()

func _on_xh_scroll_bar_value_changed(value):
	x = value
	%XCounter.text = str(value)
	update_piece_counter()

func _on_yh_scroll_bar_value_changed(value):
	y = value
	%YCounter.text = str(value)
	update_piece_counter()
