class_name PuzzleFileDialog
extends Panel

var dir := DirAccess.open("/")
var num_pictures := 0
var files : Array[String] = [] #PackedStringArray()
var load_files := 10
signal picture_directory_changed(new_dir)

func _ready():
	if not OS.request_permissions():
		assert(false, "no permissions")
	dir.include_navigational = true
	dir.include_hidden = true
	#if dir.dir_exists(Global.picture_directory):
	#	dir.change_dir(Global.picture_directory)
	#else:
	dir.change_dir(OS.get_system_dir(OS.SYSTEM_DIR_DCIM))
	change_dir(".")

## populates files values with picture paths in current directory
func get_dir_contents():
	#files = []
	var current_dir = dir.get_current_dir()
	num_pictures = 0
	for file_name in dir.get_files():
		if file_name.get_extension().to_lower() not in [
			"webp",
			"png",
			"jpg",
			"jpeg",
			]: continue
		num_pictures += 1
		if files.size() >= num_pictures:
			continue
		if files.size() < load_files:
			files.append(current_dir.path_join(file_name))
	return files

func change_dir(directory):
	dir.change_dir(directory)
	files = []
	get_dir_contents()
	%PathLabel.text = "\"{0}\"".format([dir.get_current_dir()])
	print(dir.get_current_dir())
	%AmountLabel.text = tr("CONTAINS_PICTURE_AMOUNT").format([num_pictures])
	generate_folder_buttons()
	picture_directory_changed.emit(dir.get_current_dir())

func new_folder_button(directory):
	var button = Button.new()
	button.text = " " + directory
	button.size_flags_horizontal = SIZE_EXPAND_FILL
	button.connect("pressed", Callable(self, "change_dir").bind(directory))
	button.clip_text = true
	button.alignment = HORIZONTAL_ALIGNMENT_LEFT
#	button.instance()
	%GridContainer.add_child(button)

func generate_folder_buttons():
	for child in %GridContainer.get_children():
		%GridContainer.remove_child(child)
		child.queue_free()

	if OS.get_name() != "Linux":
		new_folder_button("..")
	for file_name in dir.get_directories():
		if file_name == ".": continue
		new_folder_button(file_name)


func _on_button_pressed():
	if dir.get_current_dir() != Global.picture_directory:
		Global.picture_directory = dir.get_current_dir()
		emit_signal("picture_directory_finalized")
	visible = false
