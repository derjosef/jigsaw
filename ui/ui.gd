extends Control

func go_back() -> void:
	if not $SetupMenu.visible:
		$SetupMenu.set_enabled(true)
	elif $SetupMenu.subwindows_visible:
		$SetupMenu.subwindows_visible = false
	else:
		get_tree().quit()

func _notification(what: int) -> void:
	if what != NOTIFICATION_WM_GO_BACK_REQUEST: return
	go_back()

func _input(event: InputEvent) -> void:
	if not event.is_action_pressed("ui_cancel"): return
	go_back()
