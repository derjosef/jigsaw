extends Node

@onready var root = $"/root"
@onready var area = $"../UI/PuzzlePanel/CenterContainer/Panel/PuzzleManager"
@onready var placed = $"../UI/PuzzlePanel/CenterContainer/Panel/PuzzleManager/PuzzleLocation"
@onready var setup_panel = $"../UI/SetupMenu"
@onready var pause_button = $"../UI/pause_button"

var theme_light: Theme = preload("res://ui/theme_light.tres")
var theme_dark: Theme = preload("res://ui/theme_dark.tres")

var piece_size = 64
var pieces_by_touch_id = [null]


# Puzzle Data:
var pieces_x = 2
var pieces_y = 2

var texture: Resource
var picture_path = "res://icon.svg"
#OS.get_system_dir(OS.SYSTEM_DIR_DESKTOP)
var picture_directory = ""
var connect_diagonal = false
var rotation_enabled = false


func _ready() -> void:
	DisplayServer.set_system_theme_change_callback(theme_change)
	theme_change()

func theme_change():
	if (DisplayServer.is_dark_mode()):
		root.theme = theme_dark
	else:
		root.theme = theme_light

func set_screen_stretch(min_screen: Vector2):
	get_tree().root.content_scale_size = min_screen
