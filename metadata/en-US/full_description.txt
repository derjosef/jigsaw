A jigsaw puzzle game made with Godot 4 that is using your own pictures.

<b>Features:</b>
<ul>
 <li>Multitouch</li>
 <li>Straightforward interface</li>
 <li>Free from ads and other annoyances</li>
</ul>

The ‘Manage external storage’ permission is currently required to access your pictures.
Feel free to explore the source code, report bugs, and contribute to the project.
